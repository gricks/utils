module gitee.com/gricks/utils

go 1.13

require (
	gitee.com/gricks/logrus v1.0.0
	github.com/json-iterator/go v1.1.9
	github.com/smartystreets/goconvey v1.6.4
)
