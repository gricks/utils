package utils

import (
	"expvar"
	"math"
	"strconv"
	"sync"
	"sync/atomic"
)

// Counter describes a metric that accumulates values monotonically.
type Counter struct {
	v uint64
}

func (this *Counter) Set(v uint64) {
	atomic.StoreUint64(&this.v, v)
}

func (this *Counter) Inc() {
	this.Add(1)
}

func (this *Counter) Add(v uint64) {
	atomic.AddUint64(&this.v, v)
}

func (this *Counter) Value() uint64 {
	return atomic.LoadUint64(&this.v)
}

func (this *Counter) String() string {
	return strconv.FormatUint(this.Value(), 10)
}

// Gauge describes a metric that takes specific values over time.
type Gauge struct {
	v expvar.Float
}

func (this *Gauge) Set(v float64) {
	this.v.Set(v)
}

func (this *Gauge) Add(v float64) {
	this.v.Add(v)
}

func (this *Gauge) Value() float64 {
	return this.v.Value()
}

func (this *Gauge) String() string {
	return this.v.String()
}

// bucket store the count between value and next.value. [value, next.value)
type bucket struct {
	next  *bucket
	prev  *bucket
	value float64
	count float64
}

func (this *bucket) Next() *bucket { return this.next }
func (this *bucket) Prev() *bucket { return this.prev }

type bucketList struct {
	head *bucket
	tail *bucket
	size int
}

func (this *bucketList) Reset()         { this.head, this.tail, this.size = nil, nil, 0 }
func (this *bucketList) Empty() bool    { return this.head == nil }
func (this *bucketList) Front() *bucket { return this.head }
func (this *bucketList) Back() *bucket  { return this.tail }
func (this *bucketList) Size() int      { return this.size }

func (this *bucketList) PushFront(e *bucket) {
	e.next = this.head
	e.prev = nil

	if this.head != nil {
		this.head.prev = e
	} else {
		this.tail = e
	}

	this.head = e
	this.size++
}

func (this *bucketList) PushBack(e *bucket) {
	e.next = nil
	e.prev = this.tail

	if this.tail != nil {
		this.tail.next = e
	} else {
		this.head = e
	}

	this.tail = e
	this.size++
}

func (this *bucketList) PushBackList(l *bucketList) {
	if this.head == nil {
		this.head = l.head
		this.tail = l.tail
	} else if l.head != nil {
		this.tail.next = l.head
		l.head.prev = this.tail
		this.tail = l.tail
	}
	l.head = nil
	l.tail = nil
	this.size += l.Size()
}

func (this *bucketList) InsertAfter(b, e *bucket) {
	a := b.next
	e.next = a
	e.prev = b
	b.next = e

	if a != nil {
		a.prev = e
	} else {
		this.tail = e
	}
	this.size++
}

func (this *bucketList) InsertBefore(a, e *bucket) {
	b := a.prev
	e.next = a
	e.prev = b
	a.prev = e

	if b != nil {
		b.next = e
	} else {
		this.head = e
	}
	this.size++
}

func (this *bucketList) Remove(e *bucket) {
	prev := e.prev
	next := e.next

	if prev != nil {
		prev.next = next
	} else {
		this.head = next
	}

	if next != nil {
		next.prev = prev
	} else {
		this.tail = prev
	}
	this.size--
}

// Histogram implements the streaming approximate histogram metric
// See more: Apache Hive histogram_numeric & gohistogram
type Histogram struct {
	mtx   sync.Mutex
	l     bucketList
	m     int // max bucket
	total int
}

// NewHistogram returns a Histogram object with the given name and number of
// buckets in the underlying histogram object.
// 50 is a good default number of buckets.
func NewHistogram(bucket int) *Histogram {
	if bucket < 10 {
		bucket = 10
	}
	return &Histogram{
		m: bucket,
	}
}

func (this *Histogram) Add(v float64) {
	this.mtx.Lock()
	this.add(v)
	this.merge()
	this.mtx.Unlock()
}

func (this *Histogram) Addn(vs []float64) {
	this.mtx.Lock()
	for _, v := range vs {
		this.add(v)
	}
	this.merge()
	this.mtx.Unlock()
}

func (this *Histogram) add(v float64) {
	this.total++
	for e := this.l.Front(); e != nil; e = e.Next() {
		switch {
		case e.value == v:
			e.count++
			return
		case e.value > v:
			this.l.InsertBefore(e, &bucket{value: v, count: 1})
			return
		}
	}
	this.l.PushBack(&bucket{value: v, count: 1})
}

// merge adjacent bins to decrease the bin count to the maximum value
func (this *Histogram) merge() {
	for this.l.Size() > this.m {
		var (
			minDelta       = math.MaxFloat64
			minDeltaBucket *bucket
		)

		// Find closest bucket in terms of value
		for e := this.l.Front().Next(); e != nil; e = e.Next() {
			if delta := e.value - e.Prev().value; delta < minDelta {
				minDelta = delta
				minDeltaBucket = e
			}
		}

		// Merge minDeltaBucket and its Prev()
		count := minDeltaBucket.count + minDeltaBucket.Prev().count
		value := (minDeltaBucket.value*minDeltaBucket.count +
			minDeltaBucket.Prev().value*minDeltaBucket.Prev().count) / count
		minDeltaBucket.count = count
		minDeltaBucket.value = value

		// Remove
		this.l.Remove(minDeltaBucket.Prev())
	}
}

// Quantile returns the value of the quantile q, 0.0 < q < 1.0.
func (this *Histogram) Quantile(q float64) float64 {
	this.mtx.Lock()
	defer this.mtx.Unlock()
	count := q * float64(this.total)
	for e := this.l.Front(); e != nil; e = e.Next() {
		count -= e.count
		if count <= 0 {
			return e.value
		}
	}
	return -1
}

// Mean returns the sample mean of the distribution
func (this *Histogram) Mean() float64 {
	this.mtx.Lock()
	defer this.mtx.Unlock()
	if this.total == 0 {
		return 0.0
	}
	sum := 0.0
	for e := this.l.Front(); e != nil; e = e.Next() {
		sum += e.value * e.count
	}
	return sum / float64(this.total)
}

func (this *Histogram) Count() int {
	this.mtx.Lock()
	defer this.mtx.Unlock()
	return this.total
}
