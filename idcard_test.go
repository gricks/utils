package utils

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

var idCardTest = []string{
	"35070118550215464X",
	"33041118360426312X",
	"210322188803154101",
	"532131187907073370",
	"410601195110293456",
	"371321189602211484",
	"612724192806010376",
	"140827201006287416",
	"410622187205252077",
	"232304196307115480",
	"513436201412275265",
}

func TestVerifyIDCard(t *testing.T) {
	Convey("verifyIDCardCode", t, func() {
		for _, v := range idCardTest {
			So(verifyIDCardCode([]byte(v)), ShouldBeTrue)
		}
	})

	Convey("verifyIDCardBirthday", t, func() {
		for _, v := range idCardTest {
			So(verifyIDCardBirthday([]byte(v)), ShouldBeTrue)
		}
	})

	Convey("VerifyIDCard", t, func() {
		for _, v := range idCardTest {
			So(VerifyIDCard(v), ShouldBeTrue)
		}
	})
}
