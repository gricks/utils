package utils

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type student struct {
	Name  string
	Score int
}

func TestGetStructInfo(t *testing.T) {
	Convey("GetStructInfo", t, func() {
		r1 := GetStructInfo(student{})
		So(r1, ShouldResemble, &StructInfo{Name: "student", FieldNames: []string{"Name", "Score"}})

		r2 := GetStructInfo(&student{})
		So(r2, ShouldResemble, &StructInfo{Name: "student", FieldNames: []string{"Name", "Score"}})
	})
}
