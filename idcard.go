package utils

import "time"

var (
	idCardRatio         = []int{7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2}
	idCardMap           = []byte{'1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'}
	idCardLength18      = 18
	idCardBirthDayStart = 6
	idCardBirthDayEnd   = 14
)

func LeapYear(y int) bool {
	return ((y%400 == 0) || (y%4 == 0 && y%100 != 0))
}

func VerifyIDCard(id string) bool {
	b := []byte(id)
	if len(b) != idCardLength18 {
		return false
	}
	if !verifyIDCardBirthday(b) {
		return false
	}
	if !verifyIDCardCode(b) {
		return false
	}
	return true
}

// verify first before get age
func GetIDCardAge(id string) int {
	b := []byte(id)
	y, m, d := b2i(b[6:10]), b2i(b[10:12]), b2i(b[12:14])
	t := time.Now()
	ny, nm, nd := t.Date()
	age := ny - y
	if (int(nm) < m) || (int(nm) == m && nd < d) {
		age--
	}
	return age
}

func verifyIDCardBirthday(id []byte) bool {
	for _, v := range id[6:14] {
		if v > '9' || v < '0' {
			return false
		}
	}
	y, m, d := b2i(id[6:10]), b2i(id[10:12]), b2i(id[12:14])
	switch m {
	case 1, 3, 5, 7, 8, 10, 12:
		if d <= 0 || d > 31 {
			return false
		}
	case 4, 6, 9, 11:
		if d <= 0 || d > 30 {
			return false
		}
	case 2:
		feb := 28
		if LeapYear(y) {
			feb = 29
		}
		if d <= 0 || d > feb {
			return false
		}
	default:
		return false
	}
	return true
}

func verifyIDCardCode(id []byte) bool {
	return id[idCardLength18-1] == generateIDCardCode(id)
}

func generateIDCardCode(id []byte) byte {
	code := 0
	for i := 0; i < idCardLength18-1; i++ {
		code += int(id[i]-'0') * idCardRatio[i]
	}
	return idCardMap[code%11]
}

func b2i(b []byte) int {
	v := 0
	for i := 0; i < len(b); i++ {
		v = v*10 + int(b[i]-'0')
	}
	return v
}
