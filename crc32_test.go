package utils

import (
	"io/ioutil"
	"math/rand"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_Crc32(t *testing.T) {
	Convey("CRC32", t, func() {
		b, err := ioutil.ReadFile("crc32_test.go")
		So(err, ShouldBeNil)
		p := rand.Intn(len(b)-len(b)/2) + len(b)/4
		b1 := b[:p]
		b2 := b[p:]
		v1 := NewCRC(b).Value()
		v2 := NewCRC(b1).Append(b2).Value()
		So(v1, ShouldEqual, v2)
	})
}
