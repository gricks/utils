package utils

import "reflect"

type StructInfo struct {
	Name       string
	FieldNames []string
}

func GetStructInfo(v interface{}) *StructInfo {
	vt := reflect.TypeOf(v)
	if vt.Kind() == reflect.Ptr {
		vt = vt.Elem()
	}
	if vt.Kind() != reflect.Struct {
		return nil
	}
	r := &StructInfo{}
	r.Name = vt.Name()
	for i := 0; i < vt.NumField(); i++ {
		r.FieldNames = append(r.FieldNames, vt.Field(i).Name)
	}
	return r
}
