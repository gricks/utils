package utils

import (
	"os"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestWriteGoroutine(t *testing.T) {
	Convey("WriteGoroutine", t, func() {
		WriteGoroutine("dump.goroutine", 1)
		defer os.Remove("dump.goroutine")
	})
}

func TestWriteHeap(t *testing.T) {
	Convey("WriteHeap", t, func() {
		WriteHeap("dump.heap", 1)
		defer os.Remove("dump.heap")
	})
}
