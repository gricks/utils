package backoff

import (
	"math/rand"
	"sync"
	"time"
)

var (
	r  = rand.New(rand.NewSource(time.Now().UnixNano()))
	mu sync.Mutex
)

func rfloat64() float64 {
	mu.Lock()
	v := r.Float64()
	mu.Unlock()
	return v
}

// Strategy defines the methodology for backing off
type Strategy interface {
	Backoff(retries int) time.Duration
}

var DefaultStrategy = &Exponential{
	BaseDelay:  1.0 * time.Second,
	Multiplier: 1.6,
	Jitter:     0.2,
	MaxDelay:   120 * time.Second,
}

type Exponential struct {
	// BaseDelay is the amount of time to backoff after the first failure.
	BaseDelay time.Duration
	// Multiplier is the factor with which to multiply backoffs after a
	// failed retry. Should ideally be greater than 1.
	Multiplier float64
	// Jitter is the factor with which backoffs are randomized.
	Jitter float64
	// MaxDelay is the upper bound of backoff delay.
	MaxDelay time.Duration
}

func (e Exponential) Backoff(retries int) time.Duration {
	if retries == 0 {
		return e.BaseDelay
	}
	backoff, max := float64(e.BaseDelay), float64(e.MaxDelay)
	for backoff < max && retries > 0 {
		backoff *= e.Multiplier
		retries--
	}
	if backoff > max {
		backoff = max
	}
	// Randomize backoff delays so that if a cluster of requests start at
	// the same time, they won't operate in lockstep.
	backoff *= 1 + e.Jitter*(rfloat64()*2-1)
	if backoff < 0 {
		return 0
	}
	return time.Duration(backoff)
}
