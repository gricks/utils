package backoff

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_Backoff(t *testing.T) {
	Convey("Backoff", t, func() {
		b := DefaultStrategy
		So(b.Backoff(0), ShouldEqual, b.BaseDelay)
		So(b.Backoff(0), ShouldEqual, b.BaseDelay)
		So(b.Backoff(1000), ShouldBeLessThanOrEqualTo, b.MaxDelay*2)
		for i := 0; i < 100; i++ {
			t.Log(i, b.Backoff(i))
		}
	})
}
