package utils

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestTime(t *testing.T) {
	Convey("ParseTime", t, func() {
		n := time.Now().Truncate(time.Second)
		s := FormatTime(n)
		p, err := ParseTime(s)
		So(err, ShouldBeNil)
		So(p.Unix(), ShouldEqual, n.Unix())
	})

	Convey("ParseDate", t, func() {
		n := time.Now().Truncate(time.Hour)
		n = n.Add(-time.Hour * time.Duration(n.Hour()))
		s := FormatDate(n)
		p, err := ParseDate(s)
		So(err, ShouldBeNil)
		So(p.Unix(), ShouldEqual, n.Unix())
	})
}
