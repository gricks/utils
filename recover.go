package utils

import (
	"runtime"
	"strings"
)

type LoggerI interface {
	Error(format string, v ...interface{})
}

func Recover(logger LoggerI, wrong *bool) {
	if err := recover(); err != nil {
		if logger != nil {
			logger.Error("Err: %s", err)
			for i := 0; i < 10; i++ {
				pc, file, line, ok := runtime.Caller(i)
				if ok {
					p := strings.Index(file, "/src/")
					if p != -1 {
						file = file[p+len("/src/"):]
					}
					if logger != nil {
						logger.Error("frame %d:[func:%s,file:%s,line:%d]",
							i, runtime.FuncForPC(pc).Name(), file, line)
					}
				} else {
					break
				}
			}
		}
		if wrong != nil {
			*wrong = true
		}
	} else {
		if wrong != nil {
			*wrong = false
		}
	}
}
