package utils

import (
	"strconv"
	"sync"
)

// A Pool is a type-safe wrapper around a sync.Pool.
type BufferPool struct {
	p *sync.Pool
}

// NewBufferPool constructs a new Pool.
func NewBufferPool(capacity int) *BufferPool {
	return &BufferPool{p: &sync.Pool{
		New: func() interface{} {
			return &Buffer{bs: make([]byte, 0, capacity)}
		},
	}}
}

// Get retrieves a Buffer from the pool, creating one if necessary.
func (p *BufferPool) Get() *Buffer {
	buf := p.p.Get().(*Buffer)
	buf.Reset()
	buf.pool = p
	return buf
}

// Buffer is a thin wrapper around a byte slice. It's intended to be pooled, so
// the only way to construct one is via a Pool.
type Buffer struct {
	bs   []byte
	pool *BufferPool
}

// AppendByte writes a single byte to the Buffer.
func (b *Buffer) AppendByte(v byte) *Buffer {
	b.bs = append(b.bs, v)
	return b
}

// AppendString writes a string to the Buffer.
func (b *Buffer) AppendString(s string) *Buffer {
	b.bs = append(b.bs, s...)
	return b
}

// AppendInt appends an integer to the underlying buffer (assuming base 10).
func (b *Buffer) AppendInt(i int64) *Buffer {
	b.bs = strconv.AppendInt(b.bs, i, 10)
	return b
}

// AppendUint appends an unsigned integer to the underlying buffer (assuming
// base 10).
func (b *Buffer) AppendUint(i uint64) *Buffer {
	b.bs = strconv.AppendUint(b.bs, i, 10)
	return b
}

// AppendBool appends a bool to the underlying buffer.
func (b *Buffer) AppendBool(v bool) *Buffer {
	b.bs = strconv.AppendBool(b.bs, v)
	return b
}

// AppendFloat appends a float to the underlying buffer. It doesn't quote NaN
// or +/- Inf.
func (b *Buffer) AppendFloat32(f float32) *Buffer {
	b.bs = strconv.AppendFloat(b.bs, float64(f), 'f', -1, 32)
	return b
}

func (b *Buffer) AppendFloat64(f float64) *Buffer {
	b.bs = strconv.AppendFloat(b.bs, f, 'f', -1, 64)
	return b
}

// Len returns the length of the underlying byte slice.
func (b *Buffer) Len() int {
	return len(b.bs)
}

// Cap returns the capacity of the underlying byte slice.
func (b *Buffer) Cap() int {
	return cap(b.bs)
}

// Bytes returns a mutable reference to the underlying byte slice.
func (b *Buffer) Bytes() []byte {
	return b.bs
}

// String returns a string copy of the underlying byte slice.
func (b *Buffer) String() string {
	return string(b.bs)
}

// Reset resets the underlying byte slice. Subsequent writes re-use the slice's
// backing array.
func (b *Buffer) Reset() *Buffer {
	b.bs = b.bs[:0]
	return b
}

// Write implements io.Writer.
func (b *Buffer) Write(bs []byte) (int, error) {
	b.bs = append(b.bs, bs...)
	return len(bs), nil
}

// TrimNewline trims any final "\n" byte from the end of the buffer.
func (b *Buffer) TrimNewline() *Buffer {
	if i := len(b.bs) - 1; i >= 0 {
		if b.bs[i] == '\n' {
			b.bs = b.bs[:i]
		}
	}
	return b
}

// Free returns the Buffer to its Pool.
//
// NOTICE: Callers must not retain references to the Buffer after calling Free.
func (b *Buffer) Free() {
	b.pool.p.Put(b)
}
