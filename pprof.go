package utils

import (
	"fmt"

	"net/http"
	_ "net/http/pprof"
)

func PProf(addr string) {
	go func() {
		if err := http.ListenAndServe(addr, nil); err != nil {
			fmt.Println("Perf Start Failed|Err:", err)
		}
	}()
}
