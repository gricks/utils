package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"time"
)

var (
	dingtalkURL      = "https://oapi.dingtalk.com/robot/send?access_token="
	dingtalkBodyType = "application/json;charset=UTF-8"
	dingtalkTextType = "markdown"
	dingtalkDialer   = &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
				DualStack: true,
			}).DialContext,
			MaxIdleConns:          100,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
		Timeout: 10 * time.Second,
	}
)

type dingtalkContext struct {
	MsgType  string `json:"msgtype"`
	Markdown struct {
		Title string `json:"title"`
		Text  string `json:"text"`
	} `json:"markdown"`
}

type Dingtalk struct {
	token string
}

func NewDingtalk(token string) *Dingtalk {
	return &Dingtalk{token: token}
}

func (d *Dingtalk) Send(title string, text string) error {
	data := &dingtalkContext{}
	data.MsgType = dingtalkTextType
	data.Markdown.Title = title
	data.Markdown.Text = text

	dataByte, err := json.Marshal(data)
	if err != nil {
		return err
	}

	rsp, err := dingtalkDialer.Post(dingtalkURL+d.token, dingtalkBodyType, bytes.NewReader(dataByte))
	if err != nil {
		return err
	}
	defer rsp.Body.Close()

	if rsp.StatusCode != http.StatusOK {
		return fmt.Errorf("Dingtalk unexpected response, code %d", rsp.StatusCode)
	}
	return nil
}
