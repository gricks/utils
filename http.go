package utils

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"strings"

	jsoniter "github.com/json-iterator/go"
)

// RemoteIP tries to get the remote ip and returns it or ""
func RemoteIP(r *http.Request) string {
	a := r.Header.Get("X-Real-IP")
	if a == "" {
		a = r.Header.Get("X-Forwarded-For")
	}
	if a == "" {
		a = strings.SplitN(r.RemoteAddr, ":", 2)[0]
		if a == "[" {
			a = "127.0.0.1"
		}
	}
	return a
}

// HTTP json request encode
func MakeHTTPJsonEncodeRequest(url string) func(context.Context, *http.Request, interface{}) error {
	return func(ctx context.Context, r *http.Request, req interface{}) error {
		b := &bytes.Buffer{}
		err := jsoniter.NewEncoder(b).Encode(req)
		if err != nil {
			return err
		}
		r.Body = ioutil.NopCloser(b)
		r.URL.Path = url
		return nil
	}
}

// HTTP json request decode
func MakeHTTPJsonDecodeRequest(factory func() interface{}) func(ctx context.Context, r *http.Request) (interface{}, error) {
	return func(ctx context.Context, r *http.Request) (interface{}, error) {
		req := factory()
		err := jsoniter.NewDecoder(r.Body).Decode(req)
		return req, err
	}
}

// HTTP json response encode
func MakeHTTPJsonEncodeResponse() func(context.Context, http.ResponseWriter, interface{}) error {
	return func(ctx context.Context, w http.ResponseWriter, rsp interface{}) error {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		return jsoniter.NewEncoder(w).Encode(rsp)
	}
}

// HTTP json response decode
func MakeHTTPJsonDecodeResponse(factory func() interface{}) func(context.Context, *http.Response) (interface{}, error) {
	return func(ctx context.Context, r *http.Response) (interface{}, error) {
		rsp := factory()
		err := jsoniter.NewDecoder(r.Body).Decode(rsp)
		return rsp, err
	}
}
