package utils

import (
	"bytes"
	"strings"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_BufferWrites(t *testing.T) {
	Convey("BufferWrite", t, func() {
		buffer := NewBufferPool(1024).Get()
		So(buffer.AppendByte('v').String(), ShouldEqual, "v")
		buffer.Reset()
		So(buffer.AppendString("foo").String(), ShouldEqual, "foo")
		buffer.Reset()
		So(buffer.AppendInt(43).String(), ShouldEqual, "43")
		buffer.Reset()
		So(buffer.AppendInt(-43).String(), ShouldEqual, "-43")
		buffer.Reset()
		So(buffer.AppendUint(43).String(), ShouldEqual, "43")
		buffer.Reset()
		So(buffer.AppendBool(true).String(), ShouldEqual, "true")
		buffer.Reset()
		So(buffer.AppendFloat32(float32(3.14)).String(), ShouldEqual, "3.14")
		buffer.Reset()
		So(buffer.AppendFloat64(float64(3.14)).String(), ShouldEqual, "3.14")
		buffer.Reset()
		buffer.Write([]byte("foo"))
		So(buffer.String(), ShouldEqual, "foo")
	})
}

func BenchmarkBuffers(b *testing.B) {
	// Because we use the strconv.AppendFoo functions so liberally, we can't
	// use the standard library's bytes.Buffer anyways (without incurring a
	// bunch of extra allocations). Nevertheless, let's make sure that we're
	// not losing any precious nanoseconds.
	str := strings.Repeat("a", 1024)
	slice := make([]byte, 1024)
	buf := bytes.NewBuffer(slice)
	custom := NewBufferPool(1024).Get()
	b.Run("ByteSlice", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			slice = append(slice, str...)
			slice = slice[:0]
		}
	})
	b.Run("BytesBuffer", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			buf.WriteString(str)
			buf.Reset()
		}
	})
	b.Run("CustomBuffer", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			custom.AppendString(str)
			custom.Reset()
		}
	})
}
