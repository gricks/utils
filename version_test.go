package utils

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestVersion(t *testing.T) {
	Convey("TestVersion", t, func() {
		vers := []string{"1.2.10", "1.2.1-alpha", "1.2.2+build.3e9fbc", "1.2.3-alpha+build.3e9fbc"}
		rsts := []*Version{
			{"", "", []int64{1, 2, 10}, vers[0]},
			{"", "alpha", []int64{1, 2, 1}, vers[1]},
			{"build.3e9fbc", "", []int64{1, 2, 2}, vers[2]},
			{"build.3e9fbc", "alpha", []int64{1, 2, 3}, vers[3]},
		}

		for i := 0; i < len(rsts); i++ {
			v, err := NewVersion(vers[i])
			So(err, ShouldBeNil)
			So(v, ShouldResemble, rsts[i])
		}

		sortedRsts := []*Version{rsts[1], rsts[2], rsts[3], rsts[0]}

		Versions(rsts).Sort()

		So(rsts, ShouldResemble, sortedRsts)
	})
}
