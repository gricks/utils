package utils

import (
	"sync/atomic"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_Parallel(t *testing.T) {
	Convey("Parallel", t, func() {
		n := int32(1000)
		c := 5
		p := NewParallel(c)

		vc := int32(0)
		vn := int32(0)
		for i := int32(1); i <= n; i++ {
			p.Add(1)
			go func(idx int32) {
				defer p.Done()
				atomic.AddInt32(&vn, 1)
				atomic.AddInt32(&vc, idx)
			}(i)
		}
		p.Wait()

		So(vc, ShouldEqual, ((1+n)*n)/2)
		So(vn, ShouldEqual, n)
	})
}
