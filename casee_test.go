package utils

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_CamelcaseSplit(t *testing.T) {
	Convey("CamelcaseSplit", t, func() {
		cases := []struct {
			str    string
			expect []string
		}{
			{"", []string{}},
			{"lowercase", []string{"lowercase"}},
			{"Class", []string{"Class"}},
			{"MyClass", []string{"My", "Class"}},
			{"MyC", []string{"My", "C"}},
			{"HTML", []string{"HTML"}},
			{"PDFLoader", []string{"PDF", "Loader"}},
			{"AString", []string{"A", "String"}},
			{"SimpleXMLParser", []string{"Simple", "XML", "Parser"}},
			{"vimRPCPlugin", []string{"vim", "RPC", "Plugin"}},
			{"GL11Version", []string{"GL", "11", "Version"}},
			{"99Bottles", []string{"99", "Bottles"}},
			{"May5", []string{"May", "5"}},
			{"BFG9000", []string{"BFG", "9000"}},
			{"BöseÜberraschung", []string{"Böse", "Überraschung"}},
			{"Two  spaces", []string{"Two", "  ", "spaces"}},
			{"BadUTF8\xe2\xe2\xa1", []string{"BadUTF8\xe2\xe2\xa1"}},
		}
		for _, c := range cases {
			So(CamelcaseSplit(c.str), ShouldResemble, c.expect)
		}
	})
}
