package utils

import (
	"hash/crc32"
)

var crc32Table = crc32.MakeTable(crc32.Castagnoli)

type CRC uint32

func NewCRC(b []byte) CRC {
	return CRC(0).Append(b)
}

func (c CRC) Append(b []byte) CRC {
	return CRC(crc32.Update(uint32(c), crc32Table, b))
}

func (c CRC) Value() uint32 {
	return uint32(c>>15|c<<17) + 0xa282ead8
}
