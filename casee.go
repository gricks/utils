package utils

import (
	"strings"
	"unicode"
	"unicode/utf8"
)

// Splitting rules
//
//  1) If string is not valid UTF-8, return it without splitting as
//     single item array.
//  2) Assign all unicode characters into one of 4 sets: lower case
//     letters, upper case letters, numbers, and all other characters.
//  3) Iterate through characters of string, introducing splits
//     between adjacent characters that belong to different sets.
//  4) Iterate through array of split strings, and if a given string
//     is upper case:
//       if subsequent string is lower case:
//         move last character of upper case string to beginning of
//         lower case string
func CamelcaseSplit(src string) (entries []string) {
	// don't split invalid utf8
	if !utf8.ValidString(src) {
		return []string{src}
	}
	entries = []string{}
	var runes [][]rune
	lastClass := 0
	class := 0
	// split into fields based on class of unicode character
	for _, r := range src {
		switch true {
		case unicode.IsLower(r):
			class = 1
		case unicode.IsUpper(r):
			class = 2
		case unicode.IsDigit(r):
			class = 3
		default:
			class = 4
		}
		if class == lastClass {
			runes[len(runes)-1] = append(runes[len(runes)-1], r)
		} else {
			runes = append(runes, []rune{r})
		}
		lastClass = class
	}
	// handle upper case -> lower case sequences, e.g.
	// "PDFL", "oader" -> "PDF", "Loader"
	for i := 0; i < len(runes)-1; i++ {
		if unicode.IsUpper(runes[i][0]) && unicode.IsLower(runes[i+1][0]) {
			runes[i+1] = append([]rune{runes[i][len(runes[i])-1]}, runes[i+1]...)
			runes[i] = runes[i][:len(runes[i])-1]
		}
	}
	// construct []string from results
	for _, s := range runes {
		if len(s) > 0 {
			entries = append(entries, string(s))
		}
	}
	return
}

type SplitCase int

const (
	NoneCase  SplitCase = 0
	LowerCase SplitCase = 1
	UpperCase SplitCase = 2
	TitleCase SplitCase = 3
)

// Split a string to fields
func Split2fields(s string, c SplitCase) []string {
	fields := make([]string, 0, len([]rune(s))/3)
	for _, sf := range strings.Fields(s) {
		for _, su := range strings.Split(sf, "_") {
			for _, sh := range strings.Split(su, "-") {
				for _, sc := range CamelcaseSplit(sh) {
					switch c {
					case LowerCase:
						sc = strings.ToLower(sc)
					case UpperCase:
						sc = strings.ToUpper(sc)
					case TitleCase:
						sc = strings.ToTitle(sc)
					default:
					}
					fields = append(fields, strings.ToLower(sc))
				}
			}
		}
	}
	return fields
}

// Convert argument to snake_case style string.
func ToSnakeCase(s string) string {
	if len(s) == 0 {
		return s
	}
	return strings.Join(Split2fields(s, LowerCase), "_")
}

// Convert argument to chain-case style string.
func ToChainCase(s string) string {
	if len(s) == 0 {
		return s
	}
	return strings.Join(Split2fields(s, LowerCase), "-")
}

// Convert argument to PascalCase style string
func ToPascalCase(s string) string {
	if len(s) == 0 {
		return s
	}
	return strings.Join(Split2fields(s, TitleCase), "")
}

// Convert argument to camelCase style string
func ToCamelCase(s string) string {
	if len(s) == 0 {
		return s
	}
	fields := Split2fields(s, TitleCase)
	if len(fields) > 0 {
		fields[0] = strings.ToLower(fields[0])
	}
	return strings.Join(fields, "")
}
