package utils

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_Power(t *testing.T) {
	Convey("Power", t, func() {
		So(LogRange(1, 300), ShouldResemble, []int{
			1, 2, 4, 8, 16, 32, 64, 128, 256})

		So(Ceilp2(1023), ShouldEqual, 1024)
		So(Ceilp2(1024), ShouldEqual, 1024)
		So(Ceilp2(1025), ShouldEqual, 2048)

		So(Floorp2(1023), ShouldEqual, 512)
		So(Floorp2(1024), ShouldEqual, 1024)
		So(Floorp2(1025), ShouldEqual, 1024)
	})
}
