package utils

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_Hex2Byte_Byte2Hex(t *testing.T) {
	Convey("Hex2Byte", t, func() {
		s := "ff15"
		b := Hex2Byte(s)
		So(s, ShouldEqual, Byte2Hex(b))
	})
}

func Test_IgnoreBOM(t *testing.T) {
	Convey("IgnoreBOM", t, func() {
		b1 := []byte{0xEF, 0xBB, 0xBF, 'a', 'b', 'c'}
		b2 := []byte{'a', 'b', 'c'}
		b3 := []byte{0xEF, 0xBF, 'a', 'b', 'c'}
		b4 := []byte{}
		var b5 []byte
		So(IgnoreBOM(b1), ShouldResemble, []byte{'a', 'b', 'c'})
		So(IgnoreBOM(b2), ShouldResemble, []byte{'a', 'b', 'c'})
		So(IgnoreBOM(b3), ShouldResemble, []byte{0xEF, 0xBF, 'a', 'b', 'c'})
		So(IgnoreBOM(b4), ShouldResemble, []byte{})
		So(IgnoreBOM(b5), ShouldBeNil)
	})
}
