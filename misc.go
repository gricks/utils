package utils

import (
	"crypto/md5"
	"fmt"
	"io"
	"os"
	"time"
)

type JsonTime time.Time

func (this JsonTime) MarshalJSON() ([]byte, error) {
	b := make([]byte, 0, 24)
	b = append(b, '"')
	b = time.Time(this).AppendFormat(b, "2006-01-02 15:04:05")
	b = append(b, '"')
	return b, nil
}

func (this *JsonTime) UnmarshalJSON(b []byte) error {
	t, err := time.ParseInLocation(`"2006-01-02 15:04:05"`, string(b), time.Local)
	*this = JsonTime(t)
	return err
}

func (this JsonTime) String() string {
	return time.Time(this).Format("2006-01-02 15:04:05")
}

///////////////////////////////////////////////////////////////

var bunits = [...]string{"", "Ki", "Mi", "Gi", "Ti"}

func ShortenByte(bytes int) string {
	i := 0
	for ; bytes > 1024 && i < 4; i++ {
		bytes /= 1024
	}
	return fmt.Sprintf("%d%sB", bytes, bunits[i])
}

///////////////////////////////////////////////////////////////

func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func MinInt32(a, b int32) int32 {
	if a < b {
		return a
	}
	return b
}

func MaxInt32(a, b int32) int32 {
	if a > b {
		return a
	}
	return b
}

///////////////////////////////////////////////////////////////
// Convert Excel column name to number
func Column2Number(s string) int {
	v := 0
	carry := 1
	for i := len(s) - 1; i >= 0; i-- {
		v += int(s[i]-'A'+1) * carry
		carry *= 26
	}
	return v
}

// Convert Excel number name to column
func Number2Column(i int) string {
	s := [20]byte{} // string with 20 byte is realy a huge number
	l := 0
	i--
	for i >= 0 {
		s[l] = byte(i%26) + 'A'
		l++
		i = ((i - i%26) / 26) - 1
	}
	for k := 0; k < l/2; k++ {
		s[k], s[l-1-k] = s[l-1-k], s[k]
	}
	return string(s[:l])
}

// Filehash return file md5 sum
func Filehash(p string) (string, error) {
	f, err := os.Open(p)
	if err != nil {
		return "", err
	}
	defer f.Close()

	h := md5.New()
	_, err = io.Copy(h, f)
	if err != nil {
		return "", err
	}
	return Byte2Hex(h.Sum(nil)), nil
}
