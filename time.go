package utils

import (
	"time"
)

var Now = time.Now

// Format

const (
	DateLayout = "2006-01-02"
	TimeLayout = "2006-01-02 15:04:05"
)

func ParseTime(s string) (time.Time, error) {
	return time.ParseInLocation(TimeLayout, s, time.Local)
}

func FormatTime(t time.Time) string {
	return t.Format(TimeLayout)
}

func ParseDate(s string) (time.Time, error) {
	return time.ParseInLocation(DateLayout, s, time.Local)
}

func FormatDate(t time.Time) string {
	return t.Format(DateLayout)
}

// Uptime

var uptime = time.Now()

func Uptime() time.Time {
	return uptime
}
