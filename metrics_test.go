package utils

import (
	"io/ioutil"
	"strconv"
	"strings"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func loadHistogramData() []int {
	b, err := ioutil.ReadFile("metrics_test.data")
	if err != nil {
		panic(err)
	}
	rst := []int{}
	a := strings.Fields(string(b))
	for _, v := range a {
		if v != "" {
			i, err := strconv.Atoi(v)
			if err != nil {
				panic(err)
			}
			rst = append(rst, i)
		}
	}
	return rst
}

func Test_Histogram(t *testing.T) {
	Convey("Histogram", t, func() {
		h := NewHistogram(160)
		for _, v := range loadHistogramData() {
			h.Add(float64(v))
		}
		So(h.Count(), ShouldEqual, 14999)
		So(h.Quantile(0.25), ShouldAlmostEqual, 14, 0.2)
		So(h.Quantile(0.50), ShouldAlmostEqual, 18, 0.2)
		So(h.Quantile(0.75), ShouldAlmostEqual, 22, 0.2)
	})
}
