package utils

import (
	"errors"
	"net"
)

// InterfaceIP return ip address by interface name. eg. eth0;
//
// Using net.Listen("tcp", "127.0.0.1:0") to got a random listen port
func InterfaceIP(face string) (string, error) {
	ifs, err := net.InterfaceByName(face)
	if err != nil {
		return "", err
	}
	addrs, err := ifs.Addrs()
	if err != nil {
		return "", err
	}
	for _, addr := range addrs {
		if ipnet, ok := addr.(*net.IPNet); ok {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}
	return "", errors.New("unknow interface")
}

// InternalIP get internal ip.
func InternalIP() (string, error) {
	return getInterfaceIP(true)
}

// InternalIP get internal ip.
func ExternalIP() (string, error) {
	return getInterfaceIP(false)
}

func getInterfaceIP(internal bool) (string, error) {
	inters, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, inter := range inters {
		if !isUp(inter.Flags) {
			continue
		}
		addrs, err := inter.Addrs()
		if err != nil {
			continue
		}
		for _, addr := range addrs {
			if ipnet, ok := addr.(*net.IPNet); ok {
				if ip4 := ipnet.IP.To4(); ip4 != nil {
					if isInternalIP(ip4) == internal {
						return ip4.String(), nil
					}
				}
			}
		}
	}
	return "", errors.New("not found")
}

// isUp Interface is up
func isUp(v net.Flags) bool {
	return v&net.FlagUp == net.FlagUp
}

func isInternalIP(ip net.IP) bool {
	return (ip[0] == 10) ||
		(ip[0] == 172 && ip[1] >= 16 && ip[1] <= 31) ||
		(ip[0] == 192 && ip[1] == 168)
}
