package utils

import (
	"os"
	"testing"

	"gitee.com/gricks/logrus"
)

func TestRecover(t *testing.T) {
	logger := logrus.New(logrus.WithFile("recover"))
	defer os.Remove("recover.log")
	defer logger.Close()

	entry := logger.GetEntry()
	var wrong bool
	defer func() {
		if wrong {
			entry.Error("yep, something wrong.")
		} else {
			entry.Error("wow.")
		}
	}()
	defer Recover(entry, &wrong)

	panic("deliberate error")
}
