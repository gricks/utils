package utils

import (
	"io"
	"os"
	"runtime"

	_ "unsafe"
)

//go:linkname writeGoroutine runtime/pprof.writeGoroutine
func writeGoroutine(w io.Writer, debug int) error

func WriteGoroutine(filename string, debug int) {
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return
	}
	defer f.Close()
	writeGoroutine(f, debug)
}

//go:linkname writeHeap runtime/pprof.writeHeap
func writeHeap(w io.Writer, debug int) error

func WriteHeap(filename string, debug int) {
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return
	}
	defer f.Close()
	runtime.GC()
	writeHeap(f, debug)
}
