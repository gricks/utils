package utils

import "sync"

type Parallel struct {
	sig chan struct{}
	wg  sync.WaitGroup
}

func NewParallel(n int) *Parallel {
	return &Parallel{
		sig: make(chan struct{}, n),
	}
}

func (p *Parallel) Add(n int) {
	p.wg.Add(n)
	p.sig <- struct{}{}
}

func (p *Parallel) Done() {
	<-p.sig
	p.wg.Done()
}

func (p *Parallel) Wait() {
	p.wg.Wait()
}
