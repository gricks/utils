package utils

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

type stru struct {
	JTime JsonTime
}

func Test_JsonTime(t *testing.T) {
	Convey("TestJsonTime", t, func() {
		t := time.Unix(1546272000, 0)
		stru := &stru{}
		stru.JTime = JsonTime(t)
		b, err := json.Marshal(stru)
		So(err, ShouldEqual, nil)
		So(string(b), ShouldEqual, `{"JTime":"2019-01-01 00:00:00"}`)
		err = json.Unmarshal(b, stru)
		So(err, ShouldEqual, nil)
		So(time.Time(stru.JTime).Unix(), ShouldEqual, t.Unix())
	})
}

func Test_Column2Number(t *testing.T) {
	Convey("TestColumn2Number", t, func() {
		datas := []struct {
			Num int
			Col string
		}{
			{26, "Z"},
			{51, "AY"},
			{52, "AZ"},
			{80, "CB"},
			{676, "YZ"},
			{702, "ZZ"},
			{705, "AAC"},
		}
		for _, data := range datas {
			So(Column2Number(data.Col), ShouldEqual, data.Num)
			So(Number2Column(data.Num), ShouldEqual, data.Col)
		}
	})
}

func Test_Filehash(t *testing.T) {
	Convey("TestFilehash ", t, func() {
		file, text := "_test_file_hash", []byte("xxx")
		err := ioutil.WriteFile(file, text, 0644)
		So(err, ShouldBeNil)
		defer os.Remove(file)
		str, err := Filehash(file)
		So(err, ShouldBeNil)
		So(str, ShouldEqual, "f561aaf6ef0bf14d4208bb46a4ccb3ad")
	})
}
