package utils

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"
)

var (
	// https://semver.org
	semverRegexp = regexp.MustCompile(`^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$`)
)

type Version struct {
	build      string
	prerelease string
	segments   []int64
	original   string
}

func NewVersion(v string) (*Version, error) {
	matches := semverRegexp.FindStringSubmatch(v)
	if matches == nil {
		return nil, fmt.Errorf("invalid version: `%s`", v)
	}

	ver := &Version{}
	for i := 1; i <= 3; i++ {
		val, err := strconv.ParseInt(matches[i], 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid version: `%s`, parse failed: `%v`", v, err)
		}
		ver.segments = append(ver.segments, val)
	}

	ver.prerelease = matches[4]
	ver.build = matches[5]
	ver.original = v

	return ver, nil
}

func (v *Version) Compare(o *Version) int {
	for i := 0; i < 3; i++ {
		if v.segments[i] > o.segments[i] {
			return 1
		} else if v.segments[i] < o.segments[i] {
			return -1
		}
	}
	return 0
}

func (v *Version) Build() string      { return v.build }
func (v *Version) PreRelease() string { return v.prerelease }
func (v *Version) Original() string   { return v.original }
func (v *Version) String() string     { return v.original }

// Sort

type Versions []*Version

func (v Versions) Len() int           { return len(v) }
func (v Versions) Less(i, j int) bool { return v[i].Compare(v[j]) < 0 }
func (v Versions) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v Versions) Sort()              { sort.Sort(v) }
