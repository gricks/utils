package utils

const (
	bitsize       = 32 << (^uint(0) >> 63)
	maxint        = int(1<<(bitsize-1) - 1)
	maxintHeadBit = 1 << (bitsize - 2)
)

// LogRange logarithmic range iterates from ceiled to power of two min to max
func LogRange(min, max int) []int {
	if min <= 0 {
		panic("unexpected min range")
	}
	r := []int{}
	for i := Ceilp2(min); i <= max; i <<= 1 {
		r = append(r, i)
	}
	return r
}

// Isp2 reports whether given integer is a power of two.
func Isp2(n int) bool {
	return n&(n-1) == 0
}

// Ceilp2 returns the least power of two integer value greater than
// or equal to n.
func Ceilp2(n int) int {
	if n&maxintHeadBit != 0 && n > maxintHeadBit {
		panic("argument is too large")
	}
	if n <= 2 {
		return n
	}
	n--
	n = fillBits(n)
	n++
	return n
}

// Floorp2 returns the greatest power of two integer value less than
// or equal to n.
func Floorp2(n int) int {
	if n <= 2 {
		return n
	}
	n = fillBits(n)
	n >>= 1
	n++
	return n
}

func fillBits(n int) int {
	n |= n >> 1
	n |= n >> 2
	n |= n >> 4
	n |= n >> 8
	n |= n >> 16
	n |= n >> 32
	return n
}
